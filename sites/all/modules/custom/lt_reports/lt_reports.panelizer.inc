<?php
/**
 * @file
 * lt_reports.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function lt_reports_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:report:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'report';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'page_default';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'top' => NULL,
      'content_header' => NULL,
      'content_top' => NULL,
      'content' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'e95f575e-36b7-4d72-9947-07b544127734';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0538be85-0c76-4cb5-8704-07f40dd60e09';
    $pane->panel = 'content';
    $pane->type = 'entity_view';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'panelizer',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0538be85-0c76-4cb5-8704-07f40dd60e09';
    $display->content['new-0538be85-0c76-4cb5-8704-07f40dd60e09'] = $pane;
    $display->panels['content'][0] = 'new-0538be85-0c76-4cb5-8704-07f40dd60e09';
    $pane = new stdClass();
    $pane->pid = 'new-868b023d-dbb6-4059-ac98-277c8dbce2dd';
    $pane->panel = 'content_header';
    $pane->type = 'pane_content_header';
    $pane->subtype = 'pane_content_header';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '868b023d-dbb6-4059-ac98-277c8dbce2dd';
    $display->content['new-868b023d-dbb6-4059-ac98-277c8dbce2dd'] = $pane;
    $display->panels['content_header'][0] = 'new-868b023d-dbb6-4059-ac98-277c8dbce2dd';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-0538be85-0c76-4cb5-8704-07f40dd60e09';
  $panelizer->display = $display;
  $export['node:report:default'] = $panelizer;

  return $export;
}
