<?php

/**
 * Preprocess variables for lt_reports.current_report.tpl.php.
 */
function template_preprocess_lt_reports_current_report(&$variables) {
  //$variables['report_date'] = format_date()
  $report = $variables['report'];

  $variables['body'] = field_view_field('node', $report, 'body', 'default');

  if (!empty($report->field_report_highlights[LANGUAGE_NONE])) {
    foreach($report->field_report_highlights[LANGUAGE_NONE] as $highlight) {
      $variables['highlights'][] = $highlight['value'];
    }
  }

  if (!empty($report->field_report_document[LANGUAGE_NONE])) {
    $document = reset($report->field_report_document[LANGUAGE_NONE]);

    $variables['document'] = l(
      t('Download full report'),
      file_create_url($document['uri']),
      array(
        'attributes' => array(
          'class' => 'Button',
          'target' => '_blank',
        ),
      )
    );
  }

  if (!empty($report->field_report_press_release[LANGUAGE_NONE])) {
    $press_release = reset($report->field_report_press_release[LANGUAGE_NONE]);

    $variables['press_release'] = l(
      t('Download press release'),
      file_create_url($press_release['uri']),
      array(
        'attributes' => array(
          'class' => 'Button',
          'target' => '_blank',
        ),
      )
    );
  }
}

/**
 * Preprocess variables for lt_reports.list_item.tpl.php.
 */
function template_preprocess_lt_reports_list_item(&$variables) {
  //$variables['report_date'] = format_date()
  $report = $variables['report'];

  $variables['body'] = field_view_field('node', $report, 'body', 'default');

  if (!empty($report->field_report_document[LANGUAGE_NONE])) {
    $document = reset($report->field_report_document[LANGUAGE_NONE]);

    $variables['document'] = t(
      '<a href="@url" target="_blank">Download report<span class="Icon Icon--arrowRight"></span></a>',
      array(
        '@url' => file_create_url($document['uri']),
      )
    );
  }

  if (!empty($report->field_report_press_release[LANGUAGE_NONE])) {
    $press_release = reset($report->field_report_press_release[LANGUAGE_NONE]);

    $variables['press_release'] = t(
      '<a href="@url" target="_blank">Press release<span class="Icon Icon--arrowRight"></span></a>',
      array(
        '@url' => file_create_url($press_release['uri']),
      )
    );
  }
}
