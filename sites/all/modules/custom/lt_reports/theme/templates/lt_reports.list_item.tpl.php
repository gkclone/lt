<div class="Grid Report Report-listItem">

  <div class="Grid-cell u-lg-size4of10 u-xl-size4of10">
    <h3 class="Report-title"><?php print $report->title ?></h3>
  </div>

  <?php if (!empty($document) || !empty($press_release)): ?>
  <div class="Grid-cell Report-documents u-lg-size6of10 u-xl-size6of10">
    <?php if (!empty($document)): ?>
      <div class="Report-document">
        <?php print $document ?>
      </div>
    <?php endif ?>

    <?php if (!empty($press_release)): ?>
      <div class="Report-pressRelease">
        <?php print $press_release ?>
      </div>
    <?php endif ?>
  </div>
  <?php endif ?>
</div>
