<div class="Report Report--detail">
  <h2 class="Report-title"><?php print $report->title ?></h2>

  <?php if (!empty($body)): ?>
    <div class="Report-content">
      <?php print render($body) ?>
    </div>
  <?php endif ?>

  <?php if (!empty($highlights)): ?>
    <div class="Report-highlights">
      <?php foreach($highlights as $highlight): ?>
        <div class="Report-highlight">
          <div class="Report-highlightContent">
            <?php print $highlight ?>
          </div>
        </div>
      <?php endforeach ?>
    </div>
  <?php endif ?>

  <?php if (!empty($document)): ?>
    <div class="Report-document--current">
      <?php print $document ?>
    </div>
  <?php endif ?>

  <?php if (!empty($press_release)): ?>
    <div class="Report-pressRelease--current">
      <?php print $press_release ?>
    </div>
  <?php endif ?>
</div>
