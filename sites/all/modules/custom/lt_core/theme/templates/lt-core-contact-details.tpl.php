<?php if ($show_address): ?>
<address class="Contact-address">
  <?php foreach ($address_fields as $line): ?>
    <?php if (!empty($$line)): ?>
      <?php print $$line ?><br>
    <?php endif ?>
  <?php endforeach ?>
</address>
<?php endif ?>


<div class="Contact-furtherInfo">
  <?php if (!empty($lt_core_address_telephone)): ?>
    <div class="Contact-telephone"><?php print $lt_core_address_telephone ?></div>
  <?php endif ?>

  <?php if (!empty($lt_core_address_email)): ?>
    <div class="Contact-email"><a href="mailto:<?php print $lt_core_address_email; ?>"><?php print $lt_core_address_email ?></a></div>
  <?php endif ?>

  <?php if (!empty($lt_core_address_website)): ?>
    <div class="Contact-website"><a href="<?php print $lt_core_address_website ?>" target="_blank"><?php print $lt_core_address_website ?></a></div>
  <?php endif ?>

  <?php if (!empty($twitter)): ?>
    <div class="Contact-twitter"><a href="<?php print $twitter ?>" target="_blank"><?php print t('Follow us on twitter') ?></a></div>
  <?php endif ?>
</div>

