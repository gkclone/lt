<?php
/**
 * @file
 * lt_core.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function lt_core_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'site_template_lt_default';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -100;
  $handler->conf = array(
    'title' => 'LT: Default',
    'no_blocks' => 1,
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
  );
  $display = new panels_display();
  $display->layout = 'site_lt_default';
  $display->layout_settings = array(
    'items' => array(
      'canvas' => array(
        'type' => 'row',
        'contains' => 'column',
        'children' => array(
          0 => 'main',
        ),
        'parent' => NULL,
        'class' => '',
        'column_class' => '',
        'row_class' => '',
        'region_class' => '',
        'no_scale' => FALSE,
        'fixed_width' => '',
        'column_separation' => '0.5em',
        'region_separation' => '0.5em',
        'row_separation' => '0.5em',
      ),
      'main' => array(
        'type' => 'column',
        'width' => 100,
        'width_type' => '%',
        'children' => array(
          0 => 1,
          1 => 'main-row',
        ),
        'parent' => 'canvas',
      ),
      'main-row' => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'content',
          1 => 'sidebar',
        ),
        'parent' => 'main',
        'class' => 'main-content',
      ),
      'content' => array(
        'type' => 'region',
        'title' => 'Content',
        'width' => '100',
        'width_type' => '%',
        'parent' => 'main-row',
        'class' => '',
      ),
      'sidebar' => array(
        'type' => 'region',
        'title' => 'Sidebar',
        'width' => '200',
        'width_type' => 'px',
        'parent' => 'main-row',
      ),
      1 => array(
        'type' => 'row',
        'contains' => 'region',
        'children' => array(
          0 => 'header',
        ),
        'parent' => 'main',
        'class' => 'header',
      ),
      'header' => array(
        'type' => 'region',
        'title' => 'Header',
        'width' => 100,
        'width_type' => '%',
        'parent' => '1',
      ),
    ),
  );
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'content' => NULL,
      'sidebar' => NULL,
      'header' => NULL,
      'header_top' => NULL,
      'navigation' => NULL,
      'footer' => NULL,
      'footer_bottom' => NULL,
      'top' => NULL,
      'content_top' => NULL,
      'content_bottom' => NULL,
      'secondary' => NULL,
      'tertiary' => NULL,
      'bottom' => NULL,
      'content_header' => NULL,
      'branding' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'ecc3d344-31a9-45be-96e5-5c66add61cad';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0835db20-7f3d-47cc-9d4c-55c0a10f2f51';
    $pane->panel = 'branding';
    $pane->type = 'block';
    $pane->subtype = 'lt_core-lt_core_logo_flexible';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0835db20-7f3d-47cc-9d4c-55c0a10f2f51';
    $display->content['new-0835db20-7f3d-47cc-9d4c-55c0a10f2f51'] = $pane;
    $display->panels['branding'][0] = 'new-0835db20-7f3d-47cc-9d4c-55c0a10f2f51';
    $pane = new stdClass();
    $pane->pid = 'new-881a2b9b-88f0-41bd-8c13-813ac2743d8f';
    $pane->panel = 'content';
    $pane->type = 'page_content';
    $pane->subtype = 'page_content';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_page_content_1',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '881a2b9b-88f0-41bd-8c13-813ac2743d8f';
    $display->content['new-881a2b9b-88f0-41bd-8c13-813ac2743d8f'] = $pane;
    $display->panels['content'][0] = 'new-881a2b9b-88f0-41bd-8c13-813ac2743d8f';
    $pane = new stdClass();
    $pane->pid = 'new-8fb02084-fe9a-4434-91f4-53d3b4b647a9';
    $pane->panel = 'footer';
    $pane->type = 'block';
    $pane->subtype = 'lt_core-lt_core_yougov';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '8fb02084-fe9a-4434-91f4-53d3b4b647a9';
    $display->content['new-8fb02084-fe9a-4434-91f4-53d3b4b647a9'] = $pane;
    $display->panels['footer'][0] = 'new-8fb02084-fe9a-4434-91f4-53d3b4b647a9';
    $pane = new stdClass();
    $pane->pid = 'new-fc50e2db-29a8-46b3-a341-833cab352d45';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fc50e2db-29a8-46b3-a341-833cab352d45';
    $display->content['new-fc50e2db-29a8-46b3-a341-833cab352d45'] = $pane;
    $display->panels['footer_bottom'][0] = 'new-fc50e2db-29a8-46b3-a341-833cab352d45';
    $pane = new stdClass();
    $pane->pid = 'new-7b085627-670a-4c71-8d40-e0752798c905';
    $pane->panel = 'footer_bottom';
    $pane->type = 'block';
    $pane->subtype = 'gk_core-copyright';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '7b085627-670a-4c71-8d40-e0752798c905';
    $display->content['new-7b085627-670a-4c71-8d40-e0752798c905'] = $pane;
    $display->panels['footer_bottom'][1] = 'new-7b085627-670a-4c71-8d40-e0752798c905';
    $pane = new stdClass();
    $pane->pid = 'new-b8fe05ac-1fec-4c72-a23b-63d490b41908';
    $pane->panel = 'navigation';
    $pane->type = 'block';
    $pane->subtype = 'menu_block-gk-core-main-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b8fe05ac-1fec-4c72-a23b-63d490b41908';
    $display->content['new-b8fe05ac-1fec-4c72-a23b-63d490b41908'] = $pane;
    $display->panels['navigation'][0] = 'new-b8fe05ac-1fec-4c72-a23b-63d490b41908';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-fc50e2db-29a8-46b3-a341-833cab352d45';
  $handler->conf['display'] = $display;
  $export['site_template_lt_default'] = $handler;

  return $export;
}
