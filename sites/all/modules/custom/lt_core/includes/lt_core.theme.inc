<?php

/**
 * @file
 * Theme functions for the gk contact module.
 */

/**
 * Preprocess variables for gk-contact-details.tpl.php.
 */
function template_preprocess_lt_core_contact_details(&$variables) {
  foreach ($variables['variables'] as $key => $value) {
    $variables[$key] = $value;
  }

  $variables['twitter'] = variable_get('gk_social_twitter');

  $variables['show_address'] = FALSE;

  $variables['address_fields'] = array(
    'lt_core_address_line_1',
    'lt_core_address_line_2',
    'lt_core_address_line_3',
    'lt_core_address_line_4',
    'lt_core_address_town',
    'lt_core_address_county',
    'lt_core_address_postcode',
  );

  foreach ($variables['address_fields'] as $line) {
    if (!empty($variables[$line]) && !$variables['show_address']) {
      $variables['show_address'] = TRUE;
    }
  }
}
