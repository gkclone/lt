<?php

/**
 * Preprocess variables for html.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
function lt_preprocess_html(&$variables, $hook) {
  global $is_https;

  $protocol = $is_https ? 'https' : 'http';

  // Fonts.com
  drupal_add_js($protocol . '://fast.fonts.net/jsapi/56f06093-cb04-4c88-88e7-886480b783b8.js', 'external');
  // Load Selecter jQuery plugin.
  drupal_add_css(libraries_get_path('selecter') . '/jquery.fs.selecter.css');
  drupal_add_js(libraries_get_path('selecter') . '/jquery.fs.selecter.min.js');

  // Add id to body for scrolling to top
  $variables['attributes_array']['id'] = 'top';

  drupal_add_css(drupal_get_path('theme', 'lt') . '/less/ie.less', array('browsers' => array('IE' => 'lt IE 9', '!IE' => FALSE)));
}

/**
 * Implements hook_preprocess_pane_content_header().
 */
function lt_preprocess_pane_content_header(&$variables) {
  // Disable breadcrumb.
  $variables['breadcrumb'] = '';
}

/**
 * Preprocess variables for panels-pane.tpl.php
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("panels_pane" in this case).
 */
function lt_preprocess_panels_pane(&$variables, $hook) {

  // Display only the content for the following pane types.
  $content_only_panes = array(
    'lt_reports-lt_reports_cur_report_download',
    'menu_block-gk-core-footer-menu',
    'gk_core-copyright'
  );

  if (in_array($variables['pane']->subtype, $content_only_panes)) {
    $variables['theme_hook_suggestion'] = 'box__content_only';
  }

  // Add classes to main menu.
  if ($variables['pane']->subtype == 'menu_block-gk-core-main-menu') {
    $variables['content']['#attributes']['class'][] = 'Nav--inline';

    $variables['grid_attributes_array']['class'][] = 'Grid-cell--mainMenu';
    $variables['grid_attributes_array']['class'][] = 'u-xl-size3of4';
    $variables['grid_attributes_array']['class'][] = 'u-textRight';
  }

  // Add classes to and change template of footer copyright.
  if ($variables['pane']->subtype == 'gk_core-copyright') {
    $variables['grid_attributes_array']['class'][] = 'u-textCenter';
    $variables['theme_hook_suggestion'] = 'box__content_only';
  }

  // Add classes to the flexible logo area
  if ($variables['pane']->subtype == 'lt_core-lt_core_logo_flexible') {
    $variables['grid_attributes_array']['class'][] = 'u-xl-size1of4';
  }
}


/**
 * Implements template_preprocess_HOOK() for theme_menu_tree().
 */
function lt_preprocess_menu_tree(&$variables) {
  // Add inline class for main menu.
  $variables['attributes_array']['class'][] = 'Nav--inline';
}


/**
 * Implements template_preprocess_HOOK() for hook_textarea().
 */
function lt_preprocess_textarea(&$variables) {
  unset($variables['element']['#resizable']);
}