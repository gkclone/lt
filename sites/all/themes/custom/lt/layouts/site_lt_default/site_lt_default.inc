<?php

// Plugin definition
$plugin = array(
  'title' => t('Site LT Default'),
  'category' => t('Site'),
  'icon' => 'site_lt_default.png',
  'theme' => 'minima_site_layout',
  'regions' => array(
    'branding' => t('Branding'),
    'navigation' => t('Navigation'),
    'content' => t('Content'),
    'footer' => t('Footer'),
    'footer_bottom' => t('Footer bottom'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_site_lt_default($variables) {
  $layout = array(
    'header_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'header',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--header'),
        ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutters'),
        ),
      'branding' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-size1of4'),
          ),
        '#markup' => $variables['content']['branding'],
      ),
      'navigation' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#grid_cell_attributes' => array(
          'class' => array('u-size3of4', 'u-sm-size2of4', 'u-xs-size2of4', 'u-textRight', 'u-alignBottom'),
        ),
        '#markup' => $variables['content']['navigation'],
      ),
    ),
    'main' => array(
      '#markup' => $variables['content']['content'],
    ),
    'footer_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'footer',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--footer'),
        ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutters'),
        ),
      'footer' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['footer'],
        '#grid_cell_attributes' => array(
          'class' => array('u-textCenter'),
        ),
      ),
    ),
    'footer_bottom_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--footerBottom'),
        ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutters'),
        ),
      'footer_bottom' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['footer_bottom'],
        '#grid_cell_attributes' => array(
          'class' => array('u-textCenter'),
        ),
      ),
    ),
  );

  // If current page isn't handled by page manager wrap content in a container.
  $current_page = page_manager_get_current_page();
  if (empty($current_page)) {
    $layout['main'] = array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_element' => 'main',
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--main'),
        ),
      '#grid_attributes' => array(
        'class' => array('Grid--withGutters'),
        ),
      'main' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        'content_header' => array(
          '#theme' => 'pane_content_header'
        ),
        'content' => array(
          '#markup' => $variables['content']['content'],
        ),
      ),
    );
  }

  return $layout;
}
