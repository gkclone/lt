<?php

// Plugin definition
$plugin = array(
  'title' => t('LT: Home'),
  'category' => t('Page'),
  'icon' => 'page_lt_home.png',
  'theme' => 'minima_page_layout',
  'regions' => array(
    'top' => t('Top'),
    'message' => t('Message'),
    'primary' => t('Primary'),
    'secondary' => t('Secondary'),
    'tertiary' => t('Tertiary'),
    'bottom_left' => t('Bottom Left'),
    'bottom_right' => t('Bottom Right'),
  ),
);

/**
 * Implements minima_layout_LAYOUT_NAME().
 */
function minima_layout_page_lt_home($variables) {
  $layout = array(
    'top' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--top'),
        ),
      '#grid_attributes' => array(
        'class'=> array('Grid--withGutters')
        ),
      'top' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['top'],
        '#grid_cell_attributes' => array(
          'class' => array('u-textCenter'),
        ),
      ),
    ),
    'message' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--message'),
        ),
      '#grid_attributes' => array(
        'class'=> array('Grid--withGutters')
        ),
      'message' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['message'],
        '#grid_cell_attributes' => array(
          'class' => array('u-xl-size5of10', 'u-xl-push1of10', 'u-lg-size5of10', 'u-lg-push1of10', 'u-ie-size5of10', 'u-ie-push1of10'),
        ),
      ),
    ),
    'primary' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--primary'),
        'id' => 'primary',
        ),
      '#grid_attributes' => array(
        'class'=> array('Grid--withGutters')
        ),
      'primary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['primary'],
      ),
    ),
    'secondary' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--secondary'),
        'id' => 'secondary',
        ),
      '#grid_attributes' => array('class' => array('Grid--withGutters')),
      '#grid_attributes' => array(
        'class'=> array('Grid--withGutters')
        ),
      'secondary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['secondary'],
      ),
    ),
    'tertiary' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--tertiary'),
        'id' => 'tertiary',
        ),
      '#grid_attributes' => array(
        'class'=> array('Grid--withGutters')
        ),
      'tertiary' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['tertiary'],
      ),
    ),
    'bottom_container' => array(
      '#theme_wrappers' => array('minima_grid', 'minima_container'),
      '#container_attributes' => array(
        'class' => array('Container--constrained', 'Container--bottom'),
        'id' => 'bottom',
      ),
      '#grid_attributes' => array(
        'class'=> array('Grid--withGutters'),
      ),
      'title' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => '<h2>Contact us</h2>',
      ),
      'bottom_left' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['bottom_left'],
        '#grid_cell_attributes' => array(
          'class' => array('u-lg-size5of10', 'u-xl-size5of10', 'u-ie-size5of10', 'Grid-cell--bottom'),
        ),
      ),
      'bottom_right' => array(
        '#theme_wrappers' => array('minima_grid_cell'),
        '#markup' => $variables['content']['bottom_right'],
        '#grid_cell_attributes' => array(
          'class' => array('u-lg-size4of10', 'u-xl-size4of10', 'u-lg-push1of10', 'u-xl-push1of10', 'u-ie-size4of10', 'u-ie-push1of10', 'Grid-cell--bottom'),
        ),
      ),
    ),
  );

  return $layout;
}