(function($) {
  Drupal.behaviors.lt = {
    attach: function (context, settings) {
      // Because of forms ajax and an iframe of this page itself loading into itself
      // it appears to be nigh impossible to detect whether or not we are in an iframe
      // so I must check to see if this element has already been implemented to avoid weird
      // side effect of duplicating this code.
      if ($('.top-frame-loaded').length < 1) {
        $('body').append($('<div>').addClass('top-frame-loaded').hide());

        var navOffset = 97;
        var navHeight = 55;
        var adminMenuOffset = parseInt($('body').css('margin-top'));
        var manualScroll = false;

        // Ensure external links open in a new window.
        // Declare regex for testing URLs
        var is_absolute_url = new RegExp('^http');
        var is_internal_url = new RegExp(window.location.host);
        $('a').each(function(index, el) {
          if(is_absolute_url.test($(this).attr('href')) && !is_internal_url.test($(this).attr('href')) && $(this).attr('target') != '_blank') {
            $(this).attr('target', '_blank');
          }  
        });

        // Hide pop-up messages on click
        $('body').on('click', '.AlertBox', function() {
          $(this).slideUp('fast');
          return false;
        });

        // Smooth scrolling for same-page navigation
        $('body').on('click', '.Nav--mainMenu > .Nav-item > a, .Branding-logo', function() {
          // Get targeted element
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          // If targeted element exists
          if (target.length) {
            // Get offset of targeted element
            var offset = target.offset().top;

            if (offset > navOffset) {
              offset -= navHeight;
            }

            offset -= adminMenuOffset;

            manualScroll = true;

            // Scroll to targeted element
            $('html,body').animate({
              scrollTop: offset
            }, 1000, function() { manualScroll = false; });

            // Apply classes to navigation
            $('.Nav--mainMenu .Nav-item').removeClass('is-active');
            $(this).parent().addClass('is-active');


            if (history.pushState) {
              history.pushState(null, null, '#' + target.attr('id'));
            }
            return false;
          }
        });

        var isPortable = Modernizr.mq('only screen and (max-width: 1039px)'),
            isLap      = Modernizr.mq('only screen and (max-width: 1039px) and (min-width: 600px)'),
            isPalm     = Modernizr.mq('only screen and (max-width: 599px');

        // Parallax the home page top background image.
        if (!isPalm && $('body').hasClass('Page--front') && $('.Container--top').css('background-position')) {
          var InitialTopBgPos = $('.Container--top').css('background-position').split(" ");
          var InitialTopBgPosY = parseInt(InitialTopBgPos[1]);

          $(window).scroll(function(e) {
            var scrolled = $(window).scrollTop();

            var topBgPosY = InitialTopBgPosY - (scrolled * 0.07);

            if (topBgPosY > 0) {
              $('.Container--top').css('background-position','50% ' + topBgPosY + '%');
            }
          });
        }


        // By default we will not have pinned the navigation
        var navigationIsSticky = false,
            activeId = false,
            activeAnchor = false;

        // Pin the home page navigation bar at a certain breakpoint
        $(window).scroll(function(e) {
          toggleNavigationSticky();

          var scrollTop = $(window).scrollTop();

          // Apply 'active' classes to menu items when scrolling past relevant content
          if (!isPalm && !manualScroll) {
            $('#primary, #tertiary, #bottom').each(function() {
              var elementOffset = Math.floor($(this).offset().top - navHeight - adminMenuOffset),
                  elementHeight = $(this).outerHeight(),
                  elementBottomOffset = elementHeight + elementOffset;
                  id = $(this).attr('id');

              if (scrollTop >= elementOffset && scrollTop <= elementBottomOffset && id != activeId) {
                activeId = id;

                $('.Nav--mainMenu .Nav-item').removeClass('is-active');
                activeAnchor = $('.Nav--mainMenu a[href$=#' + activeId +']');
                activeAnchor.parent().addClass('is-active');

                // TODO: Work in progress
                // if (typeof window.activeAnchorTag === 'undefined') {
                //   window.activeAnchorTag = $('<div>')
                //     .addClass('active-anchor-tag')
                //     .appendTo('.Nav--mainMenu').parent()
                //     .css({
                //       'width' : $(activeAnchor).width(),
                //       'left' : $(activeAnchor).offset().left,
                //       'top': $(activeAnchor).css('top') + $(activeAnchor).height() + 5
                //     })
                //     .show('fast');
                // } else {
                //   window.activeAnchorTag
                //     .animate({
                //       'width' : $(activeAnchor).width(),
                //       'left' : $(activeAnchor).offset().left,
                //       'top': $(activeAnchor).css('top') + $(activeAnchor).height() + 5
                //     });
                // }
              }
              else if (id == 'primary' && scrollTop < elementOffset) {
                $('.Nav--mainMenu .Nav-item').removeClass('is-active');
                activeAnchor = false;
                activeId = false;
              }
            });
          }
        });

        // Append mobile menu and menu open event handler.
        $('.Container--header .Grid').append('<div class="Grid-cell u-textCenter MenuToggle"><a href="#" class="Icon Icon--menuToggle"></a></div>');
        $('body').on('click', '.MenuToggle a', function(event) {
          event.preventDefault();
          $('.Nav--mainMenu').toggleClass('is-shown');
          $(this).parent().toggleClass('is-active');
        });

        function toggleNavigationSticky() {
          if (!isPalm) {
            if ($(window).scrollTop() >= (navOffset + adminMenuOffset)) {
              if (!navigationIsSticky) {
                $('body').addClass('is-stickyNav');
                // $('.Branding-logo--portrait').stop().hide('fast');
                // $('.Branding-logo--landscape').stop().show('fast');
                navigationIsSticky = true;
              }
            }
            else {
              if (navigationIsSticky) {
                $('body').removeClass('is-stickyNav');
                // $('.Branding-logo--landscape').stop().hide('fast');
                // $('.Branding-logo--portrait').stop().show('fast');
                navigationIsSticky = false;
              }
            }
          }
        }

        // Append previous reports link and event handler
        var previousReportsToggle = $('<div>')
          .addClass('Report-viewPrevious')
          .text('See Previous Reports')
          .append($('<i class="Icon Icon--arrowDown">'))
          .click(function() {
            $('.Reports--previous').slideToggle('fast');
            $(this).find('.Icon').toggleClass('Icon Icon--arrowDown').toggleClass('Icon Icon--arrowUp');
            return false;
          });

        $('.Reports--previous')
          .hide()
          .before(previousReportsToggle);

        // Style select form elements.
        $("select").selecter();
      }

      $('.messages').each(function() {
        if ($(this).find('.Icon--remove').length < 1) {
          $(this).append($('<span class="Icon--remove">'));
        }
      });
    }
  }
})(jQuery);
