; GK Distro
includes[gk_distro] = http://code.gkdigital.co/gk_distro/v2.0.9/gk_distro.make

; Contrib
projects[webform_ajax][subdir] = contrib
projects[webform_ajax][version] = 1.1

; Site modules
projects[gk_bugherd][type] = module
projects[gk_bugherd][subdir] = standard
projects[gk_bugherd][download][type] = git
projects[gk_bugherd][download][url] = git@bitbucket.org:greeneking/gk-bugherd.git
projects[gk_bugherd][download][branch] = 7.x-1.0

projects[gk_promotions][type] = module
projects[gk_promotions][subdir] = standard
projects[gk_promotions][download][type] = git
projects[gk_promotions][download][url] = git@bitbucket.org:greeneking/gk-promotions.git
projects[gk_promotions][download][tag] = 7.x-1.1

projects[gk_social][type] = module
projects[gk_social][subdir] = standard
projects[gk_social][download][type] = git
projects[gk_social][download][url] = git@bitbucket.org:greeneking/gk-social.git
projects[gk_social][download][tag] = 7.x-1.1

projects[gk_webform][type] = module
projects[gk_webform][subdir] = standard
projects[gk_webform][download][type] = git
projects[gk_webform][download][url] = git@bitbucket.org:greeneking/gk-webform.git
projects[gk_webform][download][branch] = 7.x-1.3
